package com.company;

import java.util.ArrayList;

public class Empresa extends Funcionario {
    private double novoSalario;
    private double taxaAjuste;
    private ArrayList<String> listanome = new ArrayList<>();
    private ArrayList<Double> listasalario = new ArrayList<>();
    private ArrayList<Double> listanovosalario = new ArrayList<>();

    public double calculaAjuste(Funcionario funcionario) {
        if (funcionario.salario <= 150) {
            taxaAjuste = 0.25;
        } else if (funcionario.salario > 150 && funcionario.salario <= 300) {
            taxaAjuste = 0.2;
        } else if (funcionario.salario > 300 && funcionario.salario <= 600) {
            taxaAjuste = 0.15;
        } else if (funcionario.salario > 600) {
            taxaAjuste = 0.1;
        }
        novoSalario = funcionario.salario + (funcionario.salario * taxaAjuste);
        armazenaInfos(funcionario);
        return novoSalario;
    }

    public void armazenaInfos(Funcionario funcionario){
        listanome.add(funcionario.nome);
        listasalario.add(funcionario.salario);
        listanovosalario.add(novoSalario);
    }

}
