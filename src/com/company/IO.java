package com.company;

import java.util.Scanner;

public class IO {
    Scanner scanner = new Scanner(System.in);

    public String getNome(){
        System.out.println("Digite o nome do funcionario:");
        String nome = scanner.next();
        return nome;
    }

    public double getSalario(){
        System.out.println("Digite o salario do funcionario:");
        double salario = scanner.nextDouble();
        return salario;
    }

    public static void exibeDados(String nome, double salario, double novoSalario){
        System.out.println(nome + " recebia R$" + salario + " e agora recebe R$" + novoSalario);
    }
}
