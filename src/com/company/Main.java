package com.company;

public class Main {

    public static void main(String[] args) {
        IO io = new IO();
        Empresa empresa = new Empresa();
        Funcionario funcionario;
        do {
            funcionario = new Funcionario(io.getNome());
            if (!funcionario.getNome().equals("FIM")) {
                funcionario.setSalario((io.getSalario()));
                IO.exibeDados(funcionario.getNome(), funcionario.getSalario(), empresa.calculaAjuste(funcionario));
            }
        } while (!funcionario.getNome().equals("FIM"));
    }
}
